<?php 

namespace VultrPrep\Contexts;

use Exception;
use VultrPrep\Interfaces\PaymentInterface;

class PaymentContext {
    /**
     * @var StrategyContract
     */
    private $strategy;
   

    /**
     * Context constructor.
     * 
     * @param StrategyContract $strategy
     * @throws Exception
     */
    public function __construct(PaymentInterface $strategy)
    {
        if (isset($this->strategy)) {
            throw new Exception("Contract is already present.");
        }
        $this->strategy = $strategy;
    }

    /**
     * Call strategy handle() method.
     */
    public function charge(float $price): string
    {
        return $this->strategy->charge($price);
    }
}