<?php

namespace VultrPrep\Interfaces;

interface PaymentInterface
{
    public function charge(float $price): string;
}