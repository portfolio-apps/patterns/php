<?php

namespace VultrPrep\Models;

class Vehicle
{
    public $make;
    public $model;

    public function __construct(string $make, string $model)
    {
        $this->make = $make;
        $this->model = $model;
    }

    public function getMakeAndModel(): string
    {
        return "{$this->make} {$this->model}";
    } 
}