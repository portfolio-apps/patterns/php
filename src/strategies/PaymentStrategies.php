<?php

namespace VultrPrep\Strategies;

use VultrPrep\Interfaces\PaymentInterface;

class StripeStrategy implements PaymentInterface
{
    public function charge(float $price): string
    {
        return "Paid using Stripe {$price}";
    }
}

class PaypalStrategy implements PaymentInterface
{
    public function charge(float $price): string
    {
        return "Paid using Paypal {$price}";
    }
}

class DefaultStrategy implements PaymentInterface
{
    public function charge(float $price): string
    {
        return "Paid using DEFAULT {$price}";
    }
}