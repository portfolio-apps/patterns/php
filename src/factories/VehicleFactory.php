<?php

namespace VultrPrep\Factories;

use VultrPrep\Models\Vehicle;

class VehicleFactory
{
    public static function create(string $make, string $model): Vehicle
    {
        return new Vehicle($make, $model);
    }
}