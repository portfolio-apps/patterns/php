<?php declare(strict_types=1);

namespace VultrPrep;

use PHPUnit\Framework\TestCase;

use VultrPrep\Contexts\PaymentContext;
use VultrPrep\Strategies\{StripeStrategy, PaypalStrategy, DefaultStrategy};

final class PaymentStrategyTest extends TestCase
{
    public function testStripeStrategy()
    {
        $payment = new PaymentContext(new StripeStrategy());
        $charge = $payment->charge(10.50);
        $this->assertEquals("Paid using Stripe 10.5", $charge);
    }

    public function testPaypalStrategy()
    {
        $payment = new PaymentContext(new PaypalStrategy());
        $charge = $payment->charge(20.48);
        $this->assertEquals("Paid using Paypal 20.48", $charge);
    }

    public function testDefaultStrategy()
    {
        $payment = new PaymentContext(new DefaultStrategy());
        $charge = $payment->charge(10.85);
        $this->assertEquals("Paid using DEFAULT 10.85", $charge);
    }
}