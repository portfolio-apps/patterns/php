<?php declare(strict_types=1);

namespace VultrPrep;

use PHPUnit\Framework\TestCase;

use VultrPrep\Models\Vehicle;
use VultrPrep\Factories\VehicleFactory;

function dummyVehicle()
{
    $newVehicle = VehicleFactory::create("Jeep", "Cherokee");
    return $newVehicle;
}

final class VehicleFactoryTest extends TestCase
{
    public function testCanCreateVehicle()
    {
        $vehicle = dummyVehicle();
        $this->assertInstanceOf(Vehicle::class, $vehicle);
    }

    public function testGetMake()
    {
        $newVehicle = dummyVehicle();
        $make = $newVehicle->make;
        $this->assertEquals("Jeep", $make);
    }

    public function testGetModel()
    {
        $newVehicle = dummyVehicle();
        $model = $newVehicle->model;
        $this->assertEquals("Cherokee", $model);
    }

    public function testGetMakeAndModel()
    {
        $newVehicle = dummyVehicle();
        $jeep = $newVehicle->getMakeAndModel();
        $this->assertEquals("Jeep Cherokee", $jeep);
    }
}