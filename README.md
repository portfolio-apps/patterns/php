# Design Patterns in PHP w/ PHPUnit

### Current patterns
- Factory
- Strategy

### Install dependencies

```bash
composer install
```

### Run Tests

```bash
bash scripts/test.sh
```
